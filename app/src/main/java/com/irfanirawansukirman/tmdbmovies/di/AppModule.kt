package com.irfanirawansukirman.tmdbmovies.di

import com.irfanirawansukirman.tmdbmovies.BuildConfig
import com.irfanirawansukirman.tmdbmovies.domain.repository.IMoviesRepository
import com.irfanirawansukirman.tmdbmovies.domain.usecase.IMovieUseCase
import com.irfanirawansukirman.tmdbmovies.domain.usecase.IMoviePopularUseCase
import com.irfanirawansukirman.tmdbmovies.domain.usecase.MovieUseCaseImpl
import com.irfanirawansukirman.tmdbmovies.domain.usecase.MoviePopularUseCaseImpl
import com.irfanirawansukirman.tmdbmovies.remote.data.remote.movies.MoviesRemoteRepositoryImpl
import com.irfanirawansukirman.tmdbmovies.remote.data.service.MoviesService
import com.irfanirawansukirman.tmdbmovies.remote.factory.ApiFactory
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit

/**
 * Created by Irfan Irawan Sukirman on 20/12/22
 * Copyright (c) 2022 PT. Sampingan Mitra Indonesia, All Rights Reserved.
 */

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    fun provideMovieRetrofit(): Retrofit = ApiFactory.build(
        BuildConfig.TMDB_API_URL, BuildConfig.TMDB_TOKEN
    )

    @Provides
    fun provideMoviesService(retrofit: Retrofit): MoviesService {
        return ApiFactory.getService(retrofit)
    }
}

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Binds
    abstract fun provideIMoviesRepository(moviesRemoteRepositoryImpl: MoviesRemoteRepositoryImpl): IMoviesRepository
}

@Module
@InstallIn(SingletonComponent::class)
abstract class UseCaseModule {

    @Binds
    abstract fun provideMoviePopularUseCaseImpl(moviePopularUseCaseImpl: MoviePopularUseCaseImpl): IMoviePopularUseCase

    @Binds
    abstract fun provideMovieDetailUseCaseImpl(movieDetailUseCaseImpl: MovieUseCaseImpl): IMovieUseCase
}