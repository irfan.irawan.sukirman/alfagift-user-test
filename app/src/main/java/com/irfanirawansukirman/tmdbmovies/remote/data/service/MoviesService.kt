package com.irfanirawansukirman.tmdbmovies.remote.data.service

import com.irfanirawansukirman.tmdbmovies.remote.data.response.Movie
import com.irfanirawansukirman.tmdbmovies.remote.data.response.MoviePopular
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
interface MoviesService {

    @GET("movie/popular")
    suspend fun getMoviePopular(@Query("page") currentPage: Int): Response<MoviePopular>

    @GET("movie/{movie_id}")
    suspend fun getMovie(
        @Path("movie_id") movieId: Int, @Query("append_to_response") appendToResponse: String
    ): Response<Movie>
}