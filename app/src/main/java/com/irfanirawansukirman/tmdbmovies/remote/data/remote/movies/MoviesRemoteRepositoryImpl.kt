package com.irfanirawansukirman.tmdbmovies.remote.data.remote.movies

import com.irfanirawansukirman.tmdbmovies.domain.repository.IMoviesRepository
import com.irfanirawansukirman.tmdbmovies.remote.data.response.Movie
import com.irfanirawansukirman.tmdbmovies.remote.data.response.MoviePopular
import com.irfanirawansukirman.tmdbmovies.remote.data.service.MoviesService
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
class MoviesRemoteRepositoryImpl @Inject constructor(private val moviesService: MoviesService) :
    IMoviesRepository {

    override suspend fun getMoviePopular(currentPage: Int): Response<MoviePopular> {
        return moviesService.getMoviePopular(currentPage)
    }

    override suspend fun getMovie(movieId: Int): Response<Movie> {
        return moviesService.getMovie(movieId, "videos,reviews")
    }
}