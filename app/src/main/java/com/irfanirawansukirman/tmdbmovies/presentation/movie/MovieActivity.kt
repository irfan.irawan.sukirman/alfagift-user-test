package com.irfanirawansukirman.tmdbmovies.presentation.movie

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import coil.load
import com.irfanirawansukirman.tmdbmovies.R
import com.irfanirawansukirman.tmdbmovies.core.util.Const
import com.irfanirawansukirman.tmdbmovies.core.util.extension.*
import com.irfanirawansukirman.tmdbmovies.databinding.ActivityMovieBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MovieActivity : AppCompatActivity(), MovieContract, MovieVideoItemListener {

    private lateinit var viewBinding: ActivityMovieBinding

    private val viewModel: MovieViewModel by viewModels()

    private var movieId = 0

    override fun onTrailerSelected(url: String) {
        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = ActivityMovieBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        setStatusBarTransparent()
        setHeaderMarginTop()
        setViewListener()

        loadObserver()

        movieId = intent?.extras?.getInt(Const.Key.MOVIE_ID, 0) ?: 0
        if (isNetworkAvailable(this)) {
            showMovieDetailContainer()
            getMovie(movieId)
        } else {
            showNoConnectionState()
        }
    }

    private fun showMovieDetailContainer() {
        viewBinding.apply {
            errorScreenState.root.isVisible = false
            clMovieContainer.isVisible = true
        }
    }

    private fun showNoConnectionState() {
        viewBinding.apply {
            clMovieContainer.isVisible = false
            errorScreenState.apply {
                root.isVisible = true
                ivError.setImageResource(R.drawable.ic_no_connection)
                tvError.text = getString(R.string.message_connection_error)
            }
        }
    }

    private fun loadObserver() {
        viewModel.movie.observe(this, ::handleMovieDetail)
    }

    private fun handleMovieDetail(movieState: MovieState) {
        // show progress state
        viewBinding.swipeRefresh.isRefreshing = movieState.isLoading

        // show data state
        if (movieState.data != null) {
            viewBinding.apply {
                ivBackdrop.load(movieState.data.backdrop_url)
                ivPoster.load(movieState.data.poster_url)
                tvTitle.text = movieState.data.title
                tvLength.text = movieState.data.length
                tvGenres.text = movieState.data.genres
                rbRating.rating = movieState.data.rating
                tvRating.text = "${String.format("%.1f", movieState.data.rating)} / 10"
                tvOverview.text = movieState.data.overview

                rvReviews.apply {
                    layoutManager = LinearLayoutManager(this@MovieActivity)
                    itemAnimator = DefaultItemAnimator()
                    adapter = MovieReviewAdapter(movieState.data.reviews)
                }

                rvVideos.apply {
                    layoutManager = LinearLayoutManager(
                        this@MovieActivity, LinearLayoutManager.HORIZONTAL, false
                    )
                    itemAnimator = DefaultItemAnimator()
                    adapter = MovieVideoAdapter(movieState.data.trailers, this@MovieActivity)
                }
            }
        } else {
            // do with empty data
        }

        // show error state
        if (movieState.error?.isNotEmpty().isTrue()) {
            toast(movieState.error.orEmpty())
        }
    }

    private fun setViewListener() {
        viewBinding.apply {
            ivBack.setOnClickListener { finish() }
            ivFavorite.setOnClickListener { toast("Coming soon") }
            swipeRefresh.setOnRefreshListener {
                swipeRefresh.isRefreshing = false

                if (isNetworkAvailable(this@MovieActivity)) {
                    showMovieDetailContainer()
                    getMovie(movieId)
                }
            }
            errorScreenState.btnAction.setOnClickListener {
                if (isNetworkAvailable(this@MovieActivity)) {
                    showMovieDetailContainer()
                    getMovie(movieId)
                }
            }
        }
    }

    private fun setHeaderMarginTop() {
        ViewCompat.setOnApplyWindowInsetsListener(viewBinding.clMovieContainer) { _, insets ->
            viewBinding.ivBack.setMargin(top = insets.systemWindowInsetTop, left = 16)
            viewBinding.ivFavorite.setMargin(top = insets.systemWindowInsetTop, right = 24)
            insets.consumeSystemWindowInsets()
        }
    }

    override fun getMovie(movieId: Int) {
        viewModel.getMovie(movieId)
    }

    companion object {

        fun newIntent(context: Context, movieId: Int): Intent {
            return Intent(context, MovieActivity::class.java).putExtra(Const.Key.MOVIE_ID, movieId)
        }
    }
}