package com.irfanirawansukirman.tmdbmovies.presentation.moviepopular

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
interface MoviePopularItemListener {

    fun onMovieSelected(movieId: Int)
}