package com.irfanirawansukirman.tmdbmovies.presentation.movie

import com.irfanirawansukirman.tmdbmovies.domain.model.MovieUI

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */

data class MovieState(
    val isLoading: Boolean = false,
    val data: MovieUI? = null,
    val error: String? = null
)