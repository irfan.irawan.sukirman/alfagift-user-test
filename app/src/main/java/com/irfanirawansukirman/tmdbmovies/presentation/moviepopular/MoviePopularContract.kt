package com.irfanirawansukirman.tmdbmovies.presentation.moviepopular

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
interface MoviePopularContract {

    fun getMoviePopular(currentPage: Int)
}