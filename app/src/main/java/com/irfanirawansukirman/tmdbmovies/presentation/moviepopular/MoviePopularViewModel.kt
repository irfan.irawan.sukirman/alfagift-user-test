package com.irfanirawansukirman.tmdbmovies.presentation.moviepopular

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.irfanirawansukirman.tmdbmovies.core.util.Resource
import com.irfanirawansukirman.tmdbmovies.domain.usecase.IMoviePopularUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
@HiltViewModel
class MoviePopularViewModel @Inject constructor(private val iMoviePopularUseCase: IMoviePopularUseCase) :
    ViewModel(), MoviePopularContract {

    private val _moviePopular = MutableLiveData<MoviePopularState>()
    val moviePopular: LiveData<MoviePopularState>
        get() = _moviePopular

    override fun getMoviePopular(currentPage: Int) {
        viewModelScope.launch {
            iMoviePopularUseCase.getMoviePopular(currentPage).collect { state ->
                when (state) {
                    is Resource.Loading -> {
                        _moviePopular.value = MoviePopularState(isLoading = true)
                    }
                    is Resource.Dismiss -> {
                        _moviePopular.value = MoviePopularState(isLoading = false)
                    }
                    is Resource.Success -> {
                        _moviePopular.value = MoviePopularState(data = state.data)
                    }
                    is Resource.Error -> {
                        _moviePopular.value =
                            MoviePopularState(isLoading = false, error = state.message.orEmpty())
                    }
                }
            }
        }
    }
}