package com.irfanirawansukirman.tmdbmovies.presentation.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.irfanirawansukirman.tmdbmovies.databinding.ItemVideoBinding
import com.irfanirawansukirman.tmdbmovies.domain.model.Youtube

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
class MovieVideoAdapter(
    private val data: List<Youtube>,
    private val movieVideoItemListener: MovieVideoItemListener
) :
    RecyclerView.Adapter<MovieVideoAdapter.ItemHolder>() {

    class ItemHolder(private val viewBinding: ItemVideoBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindItem(
            youtube: Youtube,
        ) {
            viewBinding.mcvTrailerContainer.setOnClickListener { }
        }

        fun bindItem(youtube: Youtube, movieVideoItemListener: MovieVideoItemListener) {
            viewBinding.mcvTrailerContainer.setOnClickListener {
                movieVideoItemListener.onTrailerSelected(
                    youtube.youtube_url
                )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            ItemVideoBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bindItem(data[holder.adapterPosition], movieVideoItemListener)
    }

    override fun getItemCount(): Int {
        return data.size
    }
}