package com.irfanirawansukirman.tmdbmovies.presentation.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import coil.transform.CircleCropTransformation
import com.irfanirawansukirman.tmdbmovies.databinding.ItemReviewBinding
import com.irfanirawansukirman.tmdbmovies.domain.model.Review

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
class MovieReviewAdapter(
    private val data: List<Review>
) :
    RecyclerView.Adapter<MovieReviewAdapter.ItemHolder>() {

    class ItemHolder(private val viewBinding: ItemReviewBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindItem(
            review: Review,
        ) {
            viewBinding.apply {
                ivAvatar.load(review.avatar_url) {
                    transformations(CircleCropTransformation())
                }
                tvAuthor.text = review.username
                tvReview.text = review.review
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            ItemReviewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bindItem(data[holder.adapterPosition])
    }

    override fun getItemCount(): Int {
        return data.size
    }
}