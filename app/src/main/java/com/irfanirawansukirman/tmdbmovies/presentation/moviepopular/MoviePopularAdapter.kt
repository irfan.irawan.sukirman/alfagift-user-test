package com.irfanirawansukirman.tmdbmovies.presentation.moviepopular

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.irfanirawansukirman.tmdbmovies.databinding.ItemMovieBinding
import com.irfanirawansukirman.tmdbmovies.domain.model.MoviePopularUI

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
class MoviePopularAdapter(private val moviePopularItemListener: MoviePopularItemListener) :
    RecyclerView.Adapter<MoviePopularAdapter.ItemHolder>() {

    private val data = mutableListOf<MoviePopularUI>()

    fun addAll(data: List<MoviePopularUI>) {
        this.data.addAll(data)
        notifyDataSetChanged()
    }

    fun clearAll() {
        data.clear()
        notifyDataSetChanged()
    }

    class ItemHolder(private val viewBinding: ItemMovieBinding) :
        RecyclerView.ViewHolder(viewBinding.root) {

        fun bindItem(
            moviePopularUI: MoviePopularUI,
            moviePopularItemListener: MoviePopularItemListener
        ) {
            viewBinding.apply {
                ivPoster.load(moviePopularUI.poster_url)
                rbRating.rating = moviePopularUI.rating
                tvTitle.text = moviePopularUI.title

                viewListener.setOnClickListener {
                    moviePopularItemListener.onMovieSelected(
                        moviePopularUI.id
                    )
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemHolder {
        return ItemHolder(
            ItemMovieBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ItemHolder, position: Int) {
        holder.bindItem(data[holder.adapterPosition], moviePopularItemListener)
    }

    override fun getItemCount(): Int {
        return data.size
    }
}