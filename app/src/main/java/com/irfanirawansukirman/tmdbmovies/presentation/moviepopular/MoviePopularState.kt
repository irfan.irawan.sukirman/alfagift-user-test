package com.irfanirawansukirman.tmdbmovies.presentation.moviepopular

import com.irfanirawansukirman.tmdbmovies.data.MoviePopularUIWrapper

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */

data class MoviePopularState(
    val isLoading: Boolean = false,
    val data: MoviePopularUIWrapper? = null,
    val error: String? = null
)