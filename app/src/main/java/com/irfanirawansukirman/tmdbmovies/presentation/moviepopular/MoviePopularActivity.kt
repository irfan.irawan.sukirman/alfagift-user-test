package com.irfanirawansukirman.tmdbmovies.presentation.moviepopular

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.core.widget.NestedScrollView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import com.irfanirawansukirman.tmdbmovies.R
import com.irfanirawansukirman.tmdbmovies.core.util.extension.*
import com.irfanirawansukirman.tmdbmovies.databinding.ActivityMoviePopularBinding
import com.irfanirawansukirman.tmdbmovies.presentation.movie.MovieActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MoviePopularActivity : AppCompatActivity(), MoviePopularContract, MoviePopularItemListener {

    private lateinit var viewBinding: ActivityMoviePopularBinding

    private val viewModel: MoviePopularViewModel by viewModels()

    private val moviePopularAdapter by lazy { MoviePopularAdapter(this@MoviePopularActivity) }

    private val startPage = 1
    private var currentPage = startPage
    private var totalPage = 0
    private val pages = mutableListOf<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        viewBinding = ActivityMoviePopularBinding.inflate(layoutInflater)
        setContentView(viewBinding.root)

        loadObserver()

        setRecyclerMoviePopular()
        setViewListener()

        if (isNetworkAvailable(this)) {
            showRecyclerMovie()
            getMoviePopular(currentPage)
        } else {
            showNoConnectionState()
        }
    }

    private fun setRecyclerMoviePopular() {
        val gridLayoutManager = GridLayoutManager(this, 2)
        viewBinding.rvMoviePopular.apply {
            layoutManager = gridLayoutManager
            itemAnimator = DefaultItemAnimator()
            adapter = moviePopularAdapter
        }
    }

    private fun showRecyclerMovie() {
        viewBinding.apply {
            errorScreenState.root.hide()
            progress.hide()
            rvMoviePopular.show()
        }
    }

    private fun showNoConnectionState() {
        viewBinding.apply {
            rvMoviePopular.hide()
            errorScreenState.apply {
                root.show()
                ivError.setImageResource(R.drawable.ic_no_connection)
                tvError.text = getString(R.string.message_connection_error)
            }
        }
    }

    private fun setViewListener() {
        viewBinding.apply {
            swipeRefresh.setOnRefreshListener {
                swipeRefresh.isRefreshing = false
                currentPage = startPage
                moviePopularAdapter.clearAll()

                if (isNetworkAvailable(this@MoviePopularActivity)) {
                    showRecyclerMovie()
                    getMoviePopular(startPage)
                }
            }
            errorScreenState.btnAction.setOnClickListener {
                if (isNetworkAvailable(this@MoviePopularActivity)) {
                    moviePopularAdapter.clearAll()

                    showRecyclerMovie()
                    getMoviePopular(currentPage)
                }
            }
            nsvMoviePopularContainer.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { v, scrollX, scrollY, oldScrollX, oldScrollY ->
                if (scrollY == v.getChildAt(0).measuredHeight - v.measuredHeight) {
                    if (currentPage <= totalPage) {
                        currentPage++;
                        viewBinding.progress.show()
                        getMoviePopular(currentPage)
                    }
                }
            })
        }
    }

    private fun loadObserver() {
        viewModel.moviePopular.observe(this, ::handleMoviePopular)
    }

    private fun handleMoviePopular(moviePopularState: MoviePopularState) {
        // show progress state
        viewBinding.swipeRefresh.isRefreshing = currentPage == startPage

        pages.add(moviePopularState.data?.totalPages ?: 0)
        pages.sortDescending()
        totalPage = pages[0]

        // show data state
        moviePopularAdapter.addAll(moviePopularState.data?.movies.orEmpty())
        if (currentPage == startPage) viewBinding.swipeRefresh.isRefreshing = false

        if (moviePopularState.data?.movies?.isEmpty().isTrue()) {
            if (currentPage == startPage) {
                showEmptyState()
            }
        }

        // show error state
        if (moviePopularState.error?.isNotEmpty().isTrue()) {
            toast(moviePopularState.error.orEmpty())
        }
    }

    private fun showEmptyState() {
        viewBinding.apply {
            rvMoviePopular.isVisible = false
            errorScreenState.apply {
                root.isVisible = true
                ivError.setImageResource(R.drawable.ic_no_result)
                tvError.text = getString(R.string.message_no_movie_error)
            }
        }
    }

    override fun getMoviePopular(currentPage: Int) {
        viewModel.getMoviePopular(currentPage)
    }

    override fun onMovieSelected(movieId: Int) {
        navigateToMovie(movieId)
    }

    private fun navigateToMovie(movieId: Int) {
        startActivity(MovieActivity.newIntent(this, movieId))
    }
}