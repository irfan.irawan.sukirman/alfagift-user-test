package com.irfanirawansukirman.tmdbmovies.presentation.movie

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.irfanirawansukirman.tmdbmovies.core.util.Resource
import com.irfanirawansukirman.tmdbmovies.data.toMovieUI
import com.irfanirawansukirman.tmdbmovies.data.toReviewUI
import com.irfanirawansukirman.tmdbmovies.data.toVideoUI
import com.irfanirawansukirman.tmdbmovies.domain.usecase.IMovieUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
@HiltViewModel
class MovieViewModel @Inject constructor(
    private val iMovieUseCase: IMovieUseCase
) : ViewModel(), MovieContract {

    private val _movie = MutableLiveData<MovieState>()
    val movie: LiveData<MovieState>
        get() = _movie

    override fun getMovie(movieId: Int) {
        viewModelScope.launch {
            iMovieUseCase.getMovie(movieId).collect { state ->
                when (state) {
                    is Resource.Loading -> {
                        _movie.value = MovieState(isLoading = true)
                    }
                    is Resource.Dismiss -> {
                        _movie.value = MovieState(isLoading = false)
                    }
                    is Resource.Success -> {
                        val movieUI = state.data?.toMovieUI(
                            reviews = state.data.reviews?.results?.toReviewUI().orEmpty(),
                            trailers = state.data.videos?.results?.toVideoUI().orEmpty()
                        )
                        _movie.value = MovieState(data = movieUI)
                    }
                    is Resource.Error -> {
                        _movie.value =
                            MovieState(isLoading = false, error = state.message.orEmpty())
                    }
                }
            }
        }
    }
}