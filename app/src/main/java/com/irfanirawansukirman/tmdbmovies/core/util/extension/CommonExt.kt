package com.irfanirawansukirman.tmdbmovies.core.util.extension

import android.view.View
import androidx.core.view.isVisible

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */

fun Boolean?.isTrue(): Boolean {
    return this == true
}

fun View.show() {
    if (!isVisible) visibility = View.VISIBLE
}

fun View.hide() {
    if (isVisible) visibility = View.GONE
}