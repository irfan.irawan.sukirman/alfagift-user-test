package com.irfanirawansukirman.tmdbmovies.core.util

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
object Const {

    object Key {
        const val MOVIE_ID = "MOVIE_ID"
    }
}