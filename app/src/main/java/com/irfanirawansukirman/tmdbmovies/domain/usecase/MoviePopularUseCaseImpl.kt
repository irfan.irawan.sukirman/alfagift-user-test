package com.irfanirawansukirman.tmdbmovies.domain.usecase

import com.irfanirawansukirman.tmdbmovies.core.util.Resource
import com.irfanirawansukirman.tmdbmovies.core.util.extension.isTrue
import com.irfanirawansukirman.tmdbmovies.data.MoviePopularUIWrapper
import com.irfanirawansukirman.tmdbmovies.data.toMoviePopularUI
import com.irfanirawansukirman.tmdbmovies.remote.data.remote.movies.MoviesRemoteRepositoryImpl
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
class MoviePopularUseCaseImpl @Inject constructor(private val moviesRemoteRepositoryImpl: MoviesRemoteRepositoryImpl) :
    IMoviePopularUseCase {

    override suspend fun getMoviePopular(currentPage: Int): Flow<Resource<MoviePopularUIWrapper>> {
        return flow {
            try {
                emit(Resource.Loading())
                val response = moviesRemoteRepositoryImpl.getMoviePopular(currentPage)
                if (response.isSuccessful.isTrue()) {
                    val body = response.body()
                    val wrapper = MoviePopularUIWrapper(
                        currentPage = body?.page ?: 0,
                        totalPages = body?.totalPages ?: 0,
                        movies = body?.results?.toMoviePopularUI().orEmpty()
                    )
                    emit(Resource.Success(wrapper))
                } else {
                    emit(Resource.Error("Error Response"))
                }
                emit(Resource.Dismiss())
            } catch (e: IOException) {
                emit(Resource.Error("IO Exception: ${e.message}"))
            } catch (e: TimeoutException) {
                emit(Resource.Error("Timeout Exception: ${e.message}"))
            } catch (e: HttpException) {
                emit(Resource.Error("Http Exception: ${e.message}"))
            }
        }
    }
}