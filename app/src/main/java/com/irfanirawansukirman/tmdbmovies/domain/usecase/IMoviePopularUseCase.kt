package com.irfanirawansukirman.tmdbmovies.domain.usecase

import com.irfanirawansukirman.tmdbmovies.core.util.Resource
import com.irfanirawansukirman.tmdbmovies.data.MoviePopularUIWrapper
import kotlinx.coroutines.flow.Flow

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
interface IMoviePopularUseCase {

    suspend fun getMoviePopular(currentPage: Int): Flow<Resource<MoviePopularUIWrapper>>
}