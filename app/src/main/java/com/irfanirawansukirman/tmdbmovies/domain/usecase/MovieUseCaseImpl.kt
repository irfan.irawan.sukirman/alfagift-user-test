package com.irfanirawansukirman.tmdbmovies.domain.usecase

import com.irfanirawansukirman.tmdbmovies.core.util.Resource
import com.irfanirawansukirman.tmdbmovies.core.util.extension.isTrue
import com.irfanirawansukirman.tmdbmovies.remote.data.remote.movies.MoviesRemoteRepositoryImpl
import com.irfanirawansukirman.tmdbmovies.remote.data.response.Movie
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import java.util.concurrent.TimeoutException
import javax.inject.Inject

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
class MovieUseCaseImpl @Inject constructor(private val moviesRemoteRepositoryImpl: MoviesRemoteRepositoryImpl) :
    IMovieUseCase {

    override suspend fun getMovie(movieId: Int): Flow<Resource<Movie?>> {
        return flow {
            try {
                emit(Resource.Loading())
                val response = moviesRemoteRepositoryImpl.getMovie(movieId)
                if (response.isSuccessful.isTrue()) {
                    emit(Resource.Success(response.body()))
                } else {
                    emit(Resource.Error("Error Response"))
                }
                emit(Resource.Dismiss())
            } catch (e: IOException) {
                emit(Resource.Error("IO Exception: ${e.message}"))
            } catch (e: TimeoutException) {
                emit(Resource.Error("Timeout Exception: ${e.message}"))
            } catch (e: HttpException) {
                emit(Resource.Error("Http Exception: ${e.message}"))
            } catch (e: Exception) {
                emit(Resource.Error("Exception: ${e.message}"))
            }
        }
    }
}