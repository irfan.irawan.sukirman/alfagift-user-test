package com.irfanirawansukirman.tmdbmovies.domain.repository

import com.irfanirawansukirman.tmdbmovies.remote.data.response.Movie
import com.irfanirawansukirman.tmdbmovies.remote.data.response.MoviePopular
import retrofit2.Response

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
interface IMoviesRepository {

    suspend fun getMoviePopular(currentPage: Int): Response<out MoviePopular>

    suspend fun getMovie(movieId: Int): Response<Movie>
}