package com.irfanirawansukirman.tmdbmovies.domain.model

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */

data class Review(
    val username: String,
    val avatar_url: String,
    val created_at: String,
    val review: String
)

data class Youtube(
    val id: String,
    val youtube_url: String
)

data class MovieUI(
    val title: String,
    val poster_url: String,
    val backdrop_url: String,
    val length: String,
    val rating: Float,
    val overview: String,
    val genres: String,
    val reviews: List<Review>,
    val trailers: List<Youtube>
)