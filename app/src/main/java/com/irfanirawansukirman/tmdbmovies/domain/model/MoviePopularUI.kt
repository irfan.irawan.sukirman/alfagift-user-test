package com.irfanirawansukirman.tmdbmovies.domain.model

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */

data class MoviePopularUI(
    val id: Int,
    val title: String,
    val poster_url: String,
    val rating: Float,
    val release_date: String
)