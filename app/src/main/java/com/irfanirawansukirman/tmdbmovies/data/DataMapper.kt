package com.irfanirawansukirman.tmdbmovies.data

import com.irfanirawansukirman.tmdbmovies.core.util.extension.isTrue
import com.irfanirawansukirman.tmdbmovies.domain.model.MoviePopularUI
import com.irfanirawansukirman.tmdbmovies.domain.model.MovieUI
import com.irfanirawansukirman.tmdbmovies.domain.model.Review
import com.irfanirawansukirman.tmdbmovies.domain.model.Youtube
import com.irfanirawansukirman.tmdbmovies.remote.data.response.Movie
import com.irfanirawansukirman.tmdbmovies.remote.data.response.PopularItem
import com.irfanirawansukirman.tmdbmovies.remote.data.response.ReviewItem
import com.irfanirawansukirman.tmdbmovies.remote.data.response.VideoItem

/**
 * Created by irfanirawansukirman on 10/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */

data class MoviePopularUIWrapper(
    val currentPage: Int = 0,
    val totalPages: Int? = 0,
    val movies: List<MoviePopularUI> = listOf()
)

fun List<PopularItem>.toMoviePopularUI(
    currentPage: Int? = 0,
    totalPages: Int? = 0,
): List<MoviePopularUI> {
    val data = mutableListOf<MoviePopularUI>()

    this.forEach {
        data.add(
            MoviePopularUI(
                id = it.id ?: 0,
                title = it.title.orEmpty(),
                poster_url = "https://image.tmdb.org/t/p/w500/${it.posterPath.orEmpty()}",
                rating = (it.voteAverage?.toFloat()?.div(2)) ?: 0.0F,
                release_date = it.releaseDate.orEmpty()
            )
        )
    }

    return data
}

fun Movie.toMovieUI(reviews: List<Review>, trailers: List<Youtube>): MovieUI {
    var allGenres = ""
    genres?.forEach { allGenres += ", ${it.name}" }
    return MovieUI(
        title = title.orEmpty(),
        poster_url = "https://image.tmdb.org/t/p/w500/${posterPath}",
        backdrop_url = "https://image.tmdb.org/t/p/w500/${backdropPath}",
        length = "$runtime minutes",
        rating = (voteAverage?.toFloat()?.div(2)) ?: 0.0F,
        overview = overview.orEmpty(),
        genres = allGenres.substring(2, allGenres.length),
        reviews = reviews,
        trailers = trailers
    )
}

fun List<ReviewItem>.toReviewUI(): List<Review> {
    val data = mutableListOf<Review>()

    this.forEach {
        val avatar = if (it.authorDetails?.avatarPath?.contains("/https").isTrue()) {
            it.authorDetails?.avatarPath?.replace("/https", "https")
        } else {
            "https://image.tmdb.org/t/p/w500/${it.authorDetails?.avatarPath}"
        } ?: "-"

        data.add(
            Review(
                username = it.author.orEmpty(),
                avatar_url = avatar,
                created_at = it.createdAt.orEmpty(),
                review = it.content.orEmpty()
            )
        )
    }

    return data
}

fun List<VideoItem>.toVideoUI(): List<Youtube> {
    val data = mutableListOf<Youtube>()

    this.forEach {
        data.add(
            Youtube(
                id = it.id.orEmpty(), youtube_url = "https://www.youtube.com/watch?v=${it.key}"
            )
        )
    }

    return data
}