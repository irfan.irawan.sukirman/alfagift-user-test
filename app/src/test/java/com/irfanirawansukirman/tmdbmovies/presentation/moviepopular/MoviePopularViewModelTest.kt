package com.irfanirawansukirman.tmdbmovies.presentation.moviepopular

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.irfanirawansukirman.tmdbmovies.core.util.Resource
import com.irfanirawansukirman.tmdbmovies.fake.FakeMoviePopularUseCase
import com.irfanirawansukirman.tmdbmovies.util.DataProvider
import com.irfanirawansukirman.tmdbmovies.util.MainCoroutinesRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
@ExperimentalCoroutinesApi
class MoviePopularViewModelTest {

    @get: Rule
    val dispatcherRule = MainCoroutinesRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var fakeMoviePopularUseCase: FakeMoviePopularUseCase
    private lateinit var viewModel: MoviePopularViewModel

    @Before
    fun setup() {
        fakeMoviePopularUseCase = FakeMoviePopularUseCase()
        viewModel = MoviePopularViewModel(fakeMoviePopularUseCase)
        viewModel.getMoviePopular(DataProvider.FAKE_CURRENT_PAGE)
    }

    @Test
    fun `get movie popular is success with status code 200`() = runTest {
        fakeMoviePopularUseCase.emit(Resource.Success(DataProvider.provideFakeMoviePopularUI()))
        assertThat(viewModel.moviePopular.value?.data?.movies).isNotNull()
    }

    @Test
    fun `get movie popular is success with status code 200 but data is empty`() = runTest {
        fakeMoviePopularUseCase.emit(Resource.Success(DataProvider.provideFakeEmptyMoviePopularUI()))
        assertThat(viewModel.moviePopular.value?.data?.movies).isEmpty()
    }

    @Test
    fun `get movie popular is failed with status code 404`() = runTest {
        fakeMoviePopularUseCase.emit(Resource.Error(DataProvider.provideErrorMessage404()))
        assertThat(viewModel.moviePopular.value?.error).isEqualTo("Movie Popular Not Found")
    }
}