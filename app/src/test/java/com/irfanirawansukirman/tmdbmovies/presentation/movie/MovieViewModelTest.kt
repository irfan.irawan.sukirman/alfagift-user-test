package com.irfanirawansukirman.tmdbmovies.presentation.movie

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.irfanirawansukirman.tmdbmovies.core.util.Resource
import com.irfanirawansukirman.tmdbmovies.fake.FakeMovieUseCase
import com.irfanirawansukirman.tmdbmovies.presentation.movie.MovieViewModel
import com.irfanirawansukirman.tmdbmovies.util.DataProvider
import com.irfanirawansukirman.tmdbmovies.util.MainCoroutinesRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
@ExperimentalCoroutinesApi
class MovieViewModelTest {

    @get: Rule
    val dispatcherRule = MainCoroutinesRule()

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private lateinit var fakeMovieUseCase: FakeMovieUseCase
    private lateinit var viewModel: MovieViewModel

    @Before
    fun setup() {
        fakeMovieUseCase = FakeMovieUseCase()
        viewModel = MovieViewModel(fakeMovieUseCase)

        viewModel.getMovie(DataProvider.FAKE_MOVIE_ID)
    }

    @Test
    fun `get movie is success with status code 200`() = runTest {
        fakeMovieUseCase.emit(Resource.Success(DataProvider.provideFakeMovieResponse()))
        assertThat(viewModel.movie.value?.data).isNotNull()
    }

    @Test
    fun `get movie popular is success with status code 200 but data is empty`() = runTest {
        fakeMovieUseCase.emit(Resource.Success(null))
        assertThat(viewModel.movie.value?.data).isNull()
    }
}