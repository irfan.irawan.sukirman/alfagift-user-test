package com.irfanirawansukirman.tmdbmovies.util

import com.irfanirawansukirman.tmdbmovies.data.MoviePopularUIWrapper
import com.irfanirawansukirman.tmdbmovies.domain.model.MoviePopularUI
import com.irfanirawansukirman.tmdbmovies.remote.data.response.Genre
import com.irfanirawansukirman.tmdbmovies.remote.data.response.Movie
import com.irfanirawansukirman.tmdbmovies.remote.data.response.Reviews
import com.irfanirawansukirman.tmdbmovies.remote.data.response.Videos

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
object DataProvider {

    const val FAKE_CURRENT_PAGE = 1
    const val FAKE_MOVIE_ID = 12345
    private const val ERROR_MESSAGE_404 = "Movie Popular Not Found"

    private fun getMoviePopularUI(): MoviePopularUI {
        return MoviePopularUI(
            id = 0,
            title = "Playing With Abbas",
            poster_url = "",
            rating = 9.5F,
            release_date = ""
        )
    }

    fun provideFakeMoviePopularUI(): MoviePopularUIWrapper {
        return MoviePopularUIWrapper(
            currentPage = 1,
            totalPages = 1,
            movies = listOf(getMoviePopularUI())
        )
    }

    fun provideFakeEmptyMoviePopularUI(): MoviePopularUIWrapper {
        return MoviePopularUIWrapper(
            currentPage = 1,
            totalPages = 1,
            movies = emptyList()
        )
    }

    fun provideErrorMessage404(): String {
        return ERROR_MESSAGE_404
    }

    fun provideFakeMovieResponse(): Movie {
        return Movie(
            false,
            "",
            null,
            0,
            listOf(Genre(0, "A")),
            "",
            0,
            "",
            "",
            "",
            "",
            0.0,
            "",
            listOf(),
            listOf(),
            "",
            0,
            getReviews(),
            0,
            listOf(),
            "",
            "",
            "",
            false,
            getVideos(),
            0.0,
            0
        )
    }

    private fun getReviews(): Reviews {
        return Reviews(
            page = 1,
            results = listOf(),
            totalPages = 1,
            totalResults = 1
        )
    }

    private fun getVideos(): Videos {
        return Videos(
            results = listOf()
        )
    }
}