package com.irfanirawansukirman.tmdbmovies.fake

import com.irfanirawansukirman.tmdbmovies.core.util.Resource
import com.irfanirawansukirman.tmdbmovies.data.MoviePopularUIWrapper
import com.irfanirawansukirman.tmdbmovies.domain.usecase.IMoviePopularUseCase
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
class FakeMoviePopularUseCase : IMoviePopularUseCase {

    private val fakeMoviePopularSuccessFlow = MutableSharedFlow<Resource<MoviePopularUIWrapper>>()

    suspend fun emit(value: Resource<MoviePopularUIWrapper>) =
        fakeMoviePopularSuccessFlow.emit(value)

    override suspend fun getMoviePopular(currentPage: Int): Flow<Resource<MoviePopularUIWrapper>> {
        return fakeMoviePopularSuccessFlow
    }
}