package com.irfanirawansukirman.tmdbmovies.fake

import com.irfanirawansukirman.tmdbmovies.core.util.Resource
import com.irfanirawansukirman.tmdbmovies.domain.usecase.IMovieUseCase
import com.irfanirawansukirman.tmdbmovies.remote.data.response.Movie
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow

/**
 * Created by irfanirawansukirman on 09/01/23
 * Copyright (c) 2023 PT. Intersolusi Teknologi Asia, All Rights Reserved.
 */
class FakeMovieUseCase : IMovieUseCase {

    private val fakeMovieSuccessFlow = MutableSharedFlow<Resource<Movie?>>()

    suspend fun emit(value: Resource<Movie?>) = fakeMovieSuccessFlow.emit(value)

    override suspend fun getMovie(movieId: Int): Flow<Resource<Movie?>> {
        return fakeMovieSuccessFlow
    }
}