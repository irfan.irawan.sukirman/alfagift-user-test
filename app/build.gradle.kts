import java.io.FileInputStream
import java.util.*

plugins {
    id("com.android.application")
    id("kotlin-android")
    id("kotlin-kapt")
    id("dagger.hilt.android.plugin")
}

val prop = Properties().apply {
    load(FileInputStream(File(rootProject.rootDir, "gradle.properties")))
}

android {
    compileSdk = Android.compileSdk
    buildToolsVersion = Android.buildTools

    defaultConfig {
        applicationId = Android.applicationId
        minSdk = Android.minSdk
        targetSdk = Android.targetSdk
        versionCode = Android.versionCode
        versionName = Android.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"

        // The following argument makes the Android Test Orchestrator run its
        // "pm clear" command after each test invocation. This command ensures
        // that the app's state is completely cleared between tests.
        testInstrumentationRunnerArguments += mapOf("clearPackageData" to "true")

        buildConfigField("String", "API_URL", "\"https://hacker-news.firebaseio.com/v0/\"")
        buildConfigField("String", "TMDB_API_URL", "\"${prop.getProperty("TMDB_API_URL")}\"")
        buildConfigField("String", "TMDB_TOKEN", "\"${prop.getProperty("TMDB_TOKEN")}\"")
    }

    buildTypes {
        getByName("debug") {
            isMinifyEnabled = false
            isShrinkResources = false
            isDebuggable = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    buildFeatures {
        viewBinding = true
    }
}

dependencies {
    implementation(App.appCompat)
    implementation(App.coreKtx)
    implementation(App.viewModelKtx)
    implementation(App.swipeRefresh)
    implementation(App.materialUi)

    implementation(App.retrofit2)
    implementation(App.loggingInterceptor)
    implementation(App.okhttp)

    implementation(App.coil)

    implementation(App.coroutinesCore)
    implementation(App.coroutinesAndroid)

    implementation(App.moshiKotlin)
    implementation(App.moshi)
    implementation(App.converterMoshi)
    kapt(App.moshiKotlinCodegen)

    implementation(App.koin)

    implementation("com.google.dagger:hilt-android:2.38.1")
    kapt("com.google.dagger:hilt-compiler:2.38.1")

    testImplementation(App.junit)
    testImplementation(App.coreTesting)
    testImplementation(App.mockk)
    testImplementation(App.coroutinesTest)
    testImplementation("com.google.truth:truth:1.1.3")
    androidTestImplementation(App.espressoCore)
    androidTestImplementation(App.junitExt)
}

kapt {
    correctErrorTypes = true
}